<%@ page import="student.bean.Student" %>
<%@ page import="java.util.HashMap"  %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student</title>
<link rel="stylesheet" href="file_2.css" >
<script type="text/javascript">
function setName(){
	var name=document.getElementById("name");
	var form=document.getElementById("form_name");
	var span = name.childNodes[3];
	span.hidden="true";
	f(form);
}
function setSurname(){
	var name=document.getElementById("surname");
	var form=document.getElementById("form_surname");
	var span = name.childNodes[3];
	span.hidden="true";
	f(form);
}
function setGroup(){
	var name=document.getElementById("group");
	var form=document.getElementById("form_group");
	var span = name.childNodes[3];
	span.hidden="true";
	f(form);
}
function f(arg){
	var text=document.createElement("input");
	text.type="text";
	text.name="title"
	var submit=document.createElement("input");
	submit.type="submit";
	arg.appendChild(text);
	arg.appendChild(submit);
}

</script>

</head>
<body>
<% Student stud=(Student)request.getSession().getAttribute("student"); %>
<%if(stud==null)stud=new Student(); %>
<%HashMap<String, String> errors=(HashMap<String, String>) session.getAttribute("studErrors"); %>
<%if(errors==null) errors=new HashMap<>(); %>
  <div id="main">
     <div class="top">
        <span>Review</span>
     </div>
     <div class="data" >
        <div id="id">
           <span>ID: </span>
           <span><%out.println(stud.getId());%></span>
           <br/>
        </div>
        <div class="error"><%if(errors.containsKey("name")) out.println("* "+errors.get("name")); %></div>  
        <div id="name">
           <span >First Name: </span>
           <span ><%out.println(stud.getName()); %>
              <img onclick="setName()" src=edit.png>
           </span>
           <br/>
           <form id="form_name" action="student/setName" method="POST">
           </form>
        </div>
        <div class="error"><%if(errors.containsKey("surname")) out.println("* "+errors.get("surname")); %></div>
        <div id="surname">
           <span>Second Name: </span>
           <span><%out.println(stud.getSurname()); %>
              <img onclick="setSurname()" src=edit.png>
           </span>   
           <br/>
           <form id="form_surname" action="student/setSurname" method="POST">
           </form>
        </div>
        <div class="error"><%if(errors.containsKey("group")) out.println("* "+errors.get("group")); %></div>
        <div id="group">
           <span>Group Number: </span>
           <span><%out.println(stud.getGroup());%>
              <img onclick="setGroup()" src=edit.png>
           </span>   
           <br/>
           <form id="form_group" action="student/setGroup" method="POST">
           </form>
        </div>
     </div>   
       
     <div class="bottom">
        <div id="delete">
           <form action="student/delete" method="post">
              <input type="submit" value="delete">
           </form>   
        </div>
        <a href="student/all">All Students</a>
     </div>
   </div>  
<%errors.clear(); %>
<%session.setAttribute("studErrors", errors); %>     
</body>
</html>