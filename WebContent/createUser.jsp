<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="student.bean.User" %>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>create user</title>
<link rel="stylesheet" href="file_2.css" >
</style>
</head>
<body>
<%HashMap<String, String> errors=(HashMap<String, String>)session.getAttribute("userErrors"); %>
<%if(errors==null)errors=new HashMap<>();%>
<%User user=(User)session.getAttribute("user"); %>
<%if(user==null) {
	System.out.println("Error");
	user=new User();} %>
   <div class="top">
      <span>Create User</span> 
   </div>
   <div>
       <form action="user/create" method="post">
          <table>
             <tr>
               <td></td>
               <td class="error"><%if(errors.containsKey("login")){out.println("* "+errors.get("login"));} %></td>  
             </tr>
             <tr>
               <td><label for="login">login</label> </td>
               <td><input type="text" name="login" id="login" value="<%out.println(user.getLogin());%>"></td>  
             </tr>
             <tr>
               <td></td>
               <td class="error"><%if(errors.containsKey("pwd")){out.println("* "+errors.get("pwd"));} %></td>  
             </tr>
             <tr>
               <td><label for="pwd">password</label></td>
               <td><input type="text" name="pwd" id="pwd" value="<%out.println(user.getPassword());%>"></td>  
             </tr>
               <tr>
               <td></td>
               <td class="error"><%if(errors.containsKey("status")){out.println("* "+errors.get("status"));} %></td>  
             </tr>
             <tr>
               <td><label for="status">status</label></td>
               <td>
                  <select id="status" name="status">
                     <option value="admin">admin</option>
                     <option value="examinator">examinator</option>
                     <option value="student">student</option>
                  </select>
               </td>  
             </tr>
             <tr>
               <td></td>
               <td><input type="submit" value="create"></td>  
             </tr>
          </table>
       </form>
   </div>
   <div>
      <a href="user/all">show all users</a>
      <br/>
      <a href="welcome.jsp">main page</a>
   </div>
<%errors.clear(); %>
<%session.setAttribute("user", null); %>
<%session.setAttribute("userErrors",errors); %>     
</body>
</html>