<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="student.bean.User"  %>    
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>users</title>
<link rel="stylesheet" href="file_2.css" >
</head>
<body>
<%ArrayList<User> users=(ArrayList<User>) session.getAttribute("users"); %>
<%if(users==null) users=new ArrayList<>(); %>
     <div class="top">
          <span>List users</span>
     </div>
     <div class="center">
     <%for(User user: users){
    	out.println("<a href='user/edit?login="+user.getLogin()+"'>"); 
    	out.println(user.getLogin()); 
    	out.println("</a>");
    	out.println("<br/>"); 
     }
     %>
     <br/>
     </div>
     <div>
          <a href="user/create">create new user</a>
          <br/> 
          <a href="welcome.jsp">main page</a>
     </div>
</body>
</html>