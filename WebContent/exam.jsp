<%@ page import="student.bean.Exam" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Exam</title>
<link rel="stylesheet" href="file_2.css" >
<script type="text/javascript">

     function setSubject(){
    	 var span=document.getElementById("span_subject")
    	 var form=document.getElementById("form_subject")
    	 span.hidden="true";
    	 f(form, "text");
     }
     function setDate(){
    	 var span=document.getElementById("span_date")
    	 var form=document.getElementById("form_date")
    	 span.hidden="true";
    	 f(form, "date");
     }
     function f(arg, type){
    		var text=document.createElement("input");
    		text.type=type;
    		text.name="title"
    		var submit=document.createElement("input");
    		submit.type="submit";
    		arg.appendChild(text);
    		arg.appendChild(submit);
    	}
</script>
</head>
<body>
<%Exam exam=(Exam)request.getSession().getAttribute("exam"); %>
<%if(exam==null) exam=new Exam(); %>
<%HashMap<String, String> errors=(HashMap<String, String>) session.getAttribute("examErrors"); %>
<%if(errors==null) errors=new HashMap<>(); %>

  <div class="top">
    <span>Exam Review</span>
  </div>
  <div class="data">
     <div class="error"><%if(errors.containsKey("subject")) out.println("* "+errors.get("subject")); %></div>
     <div class="subject">
       <span>Subject: </span>
       <span id="span_subject"><%out.println(exam.getSubject()); %>
          <img src="edit.png" onclick="setSubject()">     
       </span>
       <form action="exam/setSubject" method="post" id="form_subject"></form>
     </div>
     <div class="error"><%if(errors.containsKey("date")) out.println("* "+errors.get("date")); %></div>
     <div class="date">
       <span>Date: </span>
       <span id="span_date"><%out.println(exam.getDate()); %>
          <img src="edit.png" onclick="setDate()">
       </span>
       <form action="exam/setDate" method="post" id="form_date"></form>
     </div>
  </div>
  <div class="bottom">
     <form action="exam/delete" method="post">
          <input type="submit" value="delete">
     </form>
     <a href="exam/all">All exams</a>
  </div>
<%errors.clear(); %>
<%session.setAttribute("examErrors", errors); %>     
</body>
</html>