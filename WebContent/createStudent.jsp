<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.HashMap" %>  
<%@ page import="student.bean.Student" %>  
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="file_2.css" >
</head>
<body>
<%HashMap<String, String> errors=(HashMap<String, String>)session.getAttribute("studErrors");
     if(errors==null){errors=new HashMap<>();} %>
<%Student stud=(Student) session.getAttribute("studInsert");
     if(stud==null) {stud=new Student();} %>
<div class="top">
     <span>Create student</span> 
</div>     
<div class="data"> 
  <form action="student/add" method="post">
    <table><tbody>
    <tr>
     <td></td>
     <td class="error">
        <%if(errors.containsKey("name")){ out.println("* "+errors.get("name"));} %>
     </td>
  </tr>
  <tr>
    <td><label for="name">First Name</label></td>
    <td><input type="text"  name="name" id="name" value="<%out.println(stud.getName());%>" /></td>
  </tr>
  <tr>
     <td></td>
     <td class="error">
        <%if(errors.containsKey("surname")){ out.println("* "+errors.get("surname"));} %>
     </td>
  </tr>
  <tr>
    <td><label for="surname">Second Name</label></td>
    <td><input type="text" name="surname" id="surname" value="<%out.println(stud.getSurname());%>"/></td>
  </tr>
  <tr>
     <td></td>
     <td class="error">
        <%if(errors.containsKey("group")){ out.println("* "+errors.get("group"));} %>
     </td>
  </tr>
  <tr>
    <td><label for="group">Group Number</label></td>
    <td><input type="text" name="group" id="group" value="<%out.println(stud.getGroup());%>"/></td>
  </tr>
</tbody></table>
  <br/>
  <input type="submit" value="create"/>
  </form>
</div>
<div>
   <a href="student/all">show all students</a>
   <br/>
   <a href="welcome.jsp">main page</a>
</div>
<%errors.clear();
  session.setAttribute("studError", errors); %>
<%session.setAttribute("studInsert", null); %>
</body>
</html>