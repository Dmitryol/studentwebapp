<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="student.bean.*"  %>
<%@ page import="student.dao.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
#form {
    display: normal;
}
.error{
    font-size: 12px;
    color: #ff0000;
}
.head{
    font-size: 24px;
    font-weight: bold;
}
th{
    background-color: #ff0555; 
}
tr{
    background-color: #00DD00;
}
#add{
   <%if(session.getAttribute("user_status").equals("student")) out.println("display: none"); %>
}
.delete{
    <%if(session.getAttribute("user_status").equals("student")) out.println("display: none"); %>
}
</style>
<script type="text/javascript">
</script>
</head>
<body>
<div>
<% Schedule data=new ScheduleDatabaseImpl(); %>
<% List<Event> events =data.getAllEvents(); %>
<% StudentDao stud_data=new StudDatabaseImpl(); %>
<% List<Student> students=stud_data.getAll(); %>
<% ExamDao exam_data=new ExamDatabaseImpl(); %>
<% List<Exam> exams=exam_data.getAllExam(); %>

   <div class="head">
      <span>Schedule exams</span>
   </div>
   <div>
      <table border="1">
         <tr>
            <th>Student Name</th> 
            <th>Student Surname</th>
            <th>Group Number</th> 
            <th>Subject</th>
            <th>Date</th> 
            <th>Status</th>
            <th>Score</th>
         </tr>
         <%for(Event event: events) {
        	 out.println("<tr>");
        	 out.println("<td>"+event.getStudent().getName()+"</td>");
        	 out.println("<td>"+event.getStudent().getSurname()+"</td>");
        	 out.println("<td>"+event.getStudent().getGroup()+"</td>");
        	 out.println("<td>"+event.getExam().getSubject()+"</td>");
        	 out.println("<td>"+event.getExam().getDate()+"</td>");
        	 out.println("<td>"+event.getStatus()+"</td>");
        	 out.println("<td>"+event.getScore()+"</td>");
        	 out.println("<td><form action='schedule/deleteEvent' method='post' class='delete'>"+
        	                  "<input type='number' value='"+event.getId()+"' name='id' hidden='true'>"+
        	                  "<input type='submit' value='delete'>"+
        	                  "</form></td>");
        	 out.println("</tr>");
         }
         %>
      </table>
   </div>
   <div id="add">
     <!--  <button id="button" onclick="add()">Add</button> -->
      <div class="error"><%out.println(session.getAttribute("eventErrors"));%></div>
      <form action="schedule/addEvent" method="post" id="form">
        <select name="student">
           <% for(Student stud: students){
        	  out.println("<option value="+stud.getId()+">"+stud.getSurname()+" "+stud.getName()+"</option>"); 
           } %>
        </select>
        <select name="exam">
           <% for(Exam exam: exams){
        	   out.println("<option value="+exam.getId()+">"+exam.getSubject()+"</option>");
           }
           %>
        </select>
        <select name="status">
           <option value="admitted" selected>atemmed</option>
           <option value="not admitted">not atemmed</option>
           <option value="passed">passed</option>
           <option value="not passed">not passed </option>
        </select>
        <select name="score">        
           <option value="5">5</option>
           <option value="4">4</option>
           <option value="3">3</option>
           <option value="2">2</option>
           <option value="1">1</option>
           <option value="0" selected>void</option>
        </select> 
        <input type="submit" value="OK">
      </form>
   </div>
   <div>
      <a href="welcome.jsp">Main</a>
      <button hidden="false">a</button>
   </div>
</div>
<%session.setAttribute("eventErrors", ""); %>
</body>
</html>