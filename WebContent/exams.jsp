<%@ page import="student.bean.Exam" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="file_2.css" >
</head>
<body>
<div class="top">
  <span>List Exams</span>
</div>
<div class="center">
<%List<Exam> exams=(List<Exam>)request.getSession().getAttribute("exams");
for(Exam exam: exams){
	out.println("<a href=exam/show?id="+exam.getId()+">"+exam.getSubject()+"</a><br/>");
	
}
%>
</div>
<br/><a href="exam/add">Add new exam</a>
<br/><a href="welcome.jsp">back</a>
</body>
</html>