<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="file.css" >
<title>Insert title here</title>
</head>
<body>
<h1><font color="RED">Welcome <%out.println(session.getAttribute("user_name")); %></font></h1><br/>
<%if(session.getAttribute("user_status").equals("admin")||session.getAttribute("user_status").equals("examinator")){
	out.println("<a class='link' href='student/all'>Show All Students</a>");
	out.println("<a class='link' href='exam/all'>Show All Exams</a>");
}%>
   <a class='link' href='schedule/show'>Show schedule exams</a>
<%if(session.getAttribute("user_status").equals("admin")){
	out.println("<a class='link' href='user/all'>Show all Users</a>");
}%>
   <a class='link' href='login.html'>Exit</a>
</body>
</html>