<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="student.bean.Student" %>
<%@ page import="student.dao.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="file_2.css" >
</head>
<body>
<%List<Student> students=new ArrayList<>(); %>
<%students=(List<Student>)request.getSession().getAttribute("students"); %>
 <div class="top">All students:</div>
 <div class="center">
 <% for(Student stud: students) { 	 
       out.println("<a href='student/show?id="+stud.getId()+"'>"+stud.getSurname()+" "+stud.getName()+"</a><br/>");
   }
 %>
 </div>
 <br/><a href=student/add>Add new student</a>
 <br/><a href="welcome.jsp">back</a>
</body>
</html>