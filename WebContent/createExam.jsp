<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.HashMap" %>
<%@page import="student.bean.Exam" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="file_2.css" >
</head>
<body>
<%HashMap<String, String> errors=(HashMap<String, String>)session.getAttribute("examErrors"); %>
<%if(errors==null)errors=new HashMap<>();%>
<%Exam exam=(Exam)session.getAttribute("exam"); %>
<%if(exam==null)exam=new Exam(); %>
<div class="top">
   <span>Create exam</span>
</div>
<div class="data">
   <form action="exam/add" method="post">
      <table><tbody>
         <tr class="error">
             <td></td>
             <td><%if(errors.containsKey("subject")){out.println("* "+errors.get("subject"));} %></td>
         </tr>
         <tr>
             <td><label for="subject">Subject</label></td>
             <td><input type="text" name="subject" id="subject" value="<%out.println(exam.getSubject());%>"></td>
         </tr>
         <tr class="error">
             <td></td>
             <td><%if(errors.containsKey("date")){out.println("* "+errors.get("date"));} %></td>
         </tr>
         <tr>
             <td><label for="date">Date</label></td>
             <%System.out.println(exam.getDate()); %>
             <td><input type="date" name="date" id="date" value=<%out.println(exam.getDate());%>></td>
         </tr>
         <tr>
             <td><input type="submit" value="create"></td>
             <td></td>
         </tr>   
      </tbody></table>  
   </form>
</div>
<div class="bottom"> 
   <a href="welcome.jsp">back</a>
</div>
<%errors.clear(); %>
<%session.setAttribute("exam", null); %>
<%session.setAttribute("examErrors",errors); %>  
</body>
</html>