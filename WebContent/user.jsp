<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="student.bean.User" %>
<%@ page import="java.util.HashMap" %>
    
<!DOCTYPE html PUBLIC>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>user</title>
<link rel="stylesheet" href="file_2.css" >
<script type="text/javascript">
function setLogin(){
	var name=document.getElementById("login");
	var form=document.getElementById("form_login");
	var span = name.childNodes[3];
	span.hidden="true";
	f(form);
}
function setPassword(){
	var name=document.getElementById("password");
	var form=document.getElementById("form_password");
	var span = name.childNodes[3];
	span.hidden="true";
	f(form);
}
function f(arg){
	var text=document.createElement("input");
	text.type="text";
	text.name="title"
	var submit=document.createElement("input");
	submit.type="submit";
	arg.appendChild(text);
	arg.appendChild(submit);
}
function setStatus(){
	var name=document.getElementById("status");
	var form=document.getElementById("form_status");
	var span = name.childNodes[3];
	span.hidden="true";
	
 	var select=document.createElement("select");
 	select.name="status"
	
 	var admin=document.createElement("option");
 	var examinator=document.createElement("option");
 	var student=document.createElement("option");
 	
 	admin.value="admin";
 	examinator.value= "examinator";
 	student.value= "student";
 	
 	admin.innerHTML = "admin";
 	examinator.innerHTML = "examinator";
 	student.innerHTML = "student";
	
 	select.appendChild(admin);
 	select.appendChild(examinator);
 	select.appendChild(student);
	
	var submit=document.createElement("input");
	submit.type="submit";
	form.appendChild(select);
	form.appendChild(submit);
}

</script>
</head>
<body>
<%User user=(User) session.getAttribute("user"); %>
<%if(user==null) user=new User(); %>
<%HashMap<String, String> errors=(HashMap<String, String>) session.getAttribute("userErrors"); %>
<%if(errors==null) errors=new HashMap<>(); %>
   <div class="top">
      <span>User Edit</span>
   </div>
   <div class="data">
       <div class="error"><%if(errors.containsKey("login")) out.println("* "+errors.get("login")); %></div>
       <div id="login">
           <span >Login: </span>
           <span ><%out.println(user.getLogin()); %>
              <img onclick="setLogin()" src=edit.png>
           </span>
           <br/>
           <form id="form_login" action="user/setLogin" method="POST">
           </form>
        </div>
        <div class="error"><%if(errors.containsKey("pwd")) out.println("* "+errors.get("pwd")); %></div>
        <div id="password">
           <span>Password: </span>
           <span><%out.println(user.getPassword()); %>
              <img onclick="setPassword()" src=edit.png>
           </span>   
           <br/>
           <form id="form_password" action="user/setPassword" method="POST">
           </form>
        </div>
        <div class="error"><%if(errors.containsKey("status")) out.println("* "+errors.get("status")); %></div>
        <div id="status">
           <span>Status: </span>
           <span><%out.println(user.getStatus()); %>
              <img onclick="setStatus()" src=edit.png>
           </span>   
           <br/>
           <form id="form_status" action="user/setStatus" method="POST">
           </form>
        </div>
        <div>
           <form action="user/delete" method="post">
              <input type="text" name="login" value="<%out.println(user.getLogin()); %>"  hidden="true">
              <input type="submit" value="delete">
           </form>
        </div>
   </div>
   <div>
      <a href="user/all">show all users</a>
      <br/>
      <a href="user/create">create new user</a>
      <br/>
      <a href="welcome.jsp">main page</a>
   </div>
<%errors.clear(); %>
<%session.setAttribute("userErrors", errors); %>
</body>
</html>