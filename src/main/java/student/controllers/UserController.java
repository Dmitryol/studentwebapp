package student.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import student.bean.User;
import student.dao.UserDao;
import student.dao.UserDatabaseImpl;
import student.dao.UserValidator;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/user/*")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UserDao data = new UserDatabaseImpl();
		HttpSession session=request.getSession();
		String info=request.getPathInfo();
		
		if(info.endsWith("all")) {
			
			List<User> users=data.getUsers();
			session.setAttribute("users", users);
			session.setAttribute("user", null);
			response.sendRedirect("../users.jsp");
		}
		if(info.endsWith("edit")) {
			
			User user = data.getUserByName(request.getParameter("login"));
			session.setAttribute("user", user);
			response.sendRedirect("../user.jsp");
		}
		if(info.endsWith("create")) {
			
			User user=(User)session.getAttribute("user");
			if(user==null) user=new User();
			session.setAttribute("user", user);
			response.sendRedirect("../createUser.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UserDao data = new UserDatabaseImpl();
		HttpSession session=request.getSession();
		String info=request.getPathInfo();
		
		
		if(info.endsWith("create")) {
			
			User user=new User();
			user.setLogin(request.getParameter("login"));
			user.setPassword(request.getParameter("pwd"));
			user.setStatus(request.getParameter("status"));
			
			UserValidator validator=new UserValidator();
			HashMap<String, String> userErrors=validator.userValidate(user);
			if(userErrors.isEmpty()) {
				data.createUser(user);
				response.sendRedirect("../user/all");
			}else {
				session.setAttribute("userErrors", userErrors);
				session.setAttribute("user", user);
				response.sendRedirect("../user/create");
			}
			
			
			
		}
		if(info.endsWith("delete")) {
			
			data.deleteUserByName(request.getParameter("login"));
			session.setAttribute("user", null);
			response.sendRedirect("../user/all");
			
		}
		if(info.endsWith("setLogin")) {
			
			UserValidator validator=new UserValidator();
			User user=(User)session.getAttribute("user");
			String new_login=request.getParameter("title");
			HashMap<String, String> errors=validator.loginValidate(new_login);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				
				data.loginUpdate(user.getLogin(), new_login);
				response.sendRedirect("../user/edit?login="+new_login);
				
			}else {
				session.setAttribute("userErrors", errors);
				response.sendRedirect("../user/edit?login="+user.getLogin());
			}
					
		}
        if(info.endsWith("setPassword")) {
			
			UserValidator validator=new UserValidator();
			User user=(User)session.getAttribute("user");
			String new_password=request.getParameter("title");
			HashMap<String, String> errors=validator.passwordValidate(new_password);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				
				data.passwordUpdate(user.getLogin(), new_password);
				response.sendRedirect("../user/edit?login="+user.getLogin());
				
			}else {
				session.setAttribute("userErrors", errors);
				response.sendRedirect("../user/edit?login="+user.getLogin());
			}
					
		}
        if(info.endsWith("setStatus")) {
			
			UserValidator validator=new UserValidator();
			User user=(User)session.getAttribute("user");
			String status=request.getParameter("status");
			HashMap<String, String> errors=validator.statusValidate(status);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				
				data.statusUpdate(user.getLogin(), status);
				if(user.getLogin().equals(session.getAttribute("user_name")))session.setAttribute("user_status", status );
				response.sendRedirect("../user/edit?login="+user.getLogin());
				
			}else {
				session.setAttribute("userErrors", errors);
				response.sendRedirect("../user/edit?login="+user.getLogin());
			}
					
		}
		
	}

}
