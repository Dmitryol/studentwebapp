package student.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import student.bean.Student;
import student.dao.*;

/**
 * Servlet implementation class StudentController
 */
@WebServlet("/student/*")
public class StudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentController() {
        super();
        //
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		StudentDao dao=new StudDatabaseImpl();
	    HttpSession session =request.getSession();
		String info=request.getPathInfo();
		
		
		if(info.endsWith("all")) {
			List<Student> students=dao.getAll();
			session.setAttribute("students", students);
			response.sendRedirect("../students.jsp");
		}
		if(info.endsWith("add")) {
			session.setAttribute("studError", new HashMap<String, String>());
			Student stud =(Student) session.getAttribute("studInsert");
			if(stud==null) {stud=new Student();}
			session.setAttribute("studInsert", stud);
			response.sendRedirect("../createStudent.jsp");
		}
		if(info.endsWith("show")) {
			int id=Integer.parseInt(request.getParameter("id"));
			Student stud=dao.getById(id);
			session.setAttribute("student", stud);
			response.sendRedirect("../student.jsp");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		StudentDao dao=new StudDatabaseImpl();
		String info=request.getPathInfo();
		HttpSession session=request.getSession();
		StudentValidator validator=new StudentValidator();
		
		if(info.endsWith("add")) {
			
			Student stud=new Student();
			
			stud.setName(request.getParameter("name"));
			stud.setSurname(request.getParameter("surname"));
			stud.setGroup(request.getParameter("group"));
			
			HashMap<String, String> errors=validator.validateStudent(stud);
			
			if(errors.isEmpty()) {
				dao.createStudent(stud);
				response.sendRedirect("../student/all");
			}else {
				session.setAttribute("studInsert", stud);
				session.setAttribute("studErrors", errors);
				response.sendRedirect("../student/add"); 
			}
		}
		
		
		Student stud =(Student)session.getAttribute("student");	
		
		if(info.endsWith("setName")) {
			
			String name = request.getParameter("title");
			HashMap<String, String> errors=validator.validateName(name);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				
				dao.updateName(stud.getId(), name);
				response.sendRedirect("../student/show?id="+stud.getId());
				
			}else {
				session.setAttribute("studErrors", errors);
				response.sendRedirect("../student/show?id="+stud.getId());
			}
		}
		if(info.endsWith("setSurname")) {
			
			String surname = request.getParameter("title");
			HashMap<String, String> errors=validator.validateSurname(surname);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				
				dao.updateSurname(stud.getId(), surname);
				response.sendRedirect("../student/show?id="+stud.getId());
				
			}else {
				session.setAttribute("studErrors", errors);
				response.sendRedirect("../student/show?id="+stud.getId());
			}
			
		}
        if(info.endsWith("setGroup")) {
			
        	String group = request.getParameter("title");
			HashMap<String, String> errors=validator.validateGroup(group);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				
				dao.updateGroup(stud.getId(), group);
				response.sendRedirect("../student/show?id="+stud.getId());
				
			}else {
				session.setAttribute("studErrors", errors);
				response.sendRedirect("../student/show?id="+stud.getId());
			}
			
		}
        if(info.endsWith("delete")) {
        	
        	dao.removeById(stud.getId());
        	session.setAttribute("student", null);
        	
        	response.sendRedirect("../student/all");
        }
	}
}
