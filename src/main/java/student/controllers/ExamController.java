package student.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import student.bean.Exam;
import student.dao.ExamDao;
import student.dao.ExamDatabaseImpl;
import student.dao.ExamValidator;

/**
 * Servlet implementation class ExamController
 */
@WebServlet("/exam/*")
public class ExamController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExamController() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ExamDao dao=new ExamDatabaseImpl();
		HttpSession session=request.getSession();
		String info=request.getPathInfo();
		//if(info==null)response.sendRedirect("exam/all");
		if(info.endsWith("all")) {
			
			List<Exam>exams=dao.getAllExam();
			session.setAttribute("exams", exams);
			response.sendRedirect("../exams.jsp");
			
		}else if(info.endsWith("add")) {
			
			session.setAttribute("examErrors", new HashMap<String, String>());
			Exam exam=(Exam)session.getAttribute("exam");
			if(exam==null)exam=new Exam();
			session.setAttribute("exam", exam);
			response.sendRedirect("../createExam.jsp");
			
		}else if(info.endsWith("show")) {
			int id=Integer.parseInt(request.getParameter("id"));
			Exam exam=dao.getExamById(id);
			session.setAttribute("exam", exam);
			response.sendRedirect("../exam.jsp");
			
		}else {
			response.sendRedirect("../student");
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ExamDao dao=new ExamDatabaseImpl();
		ExamValidator validator=new ExamValidator();
		String info=request.getPathInfo();
		HttpSession session=request.getSession();
		
		if(info.endsWith("add")) {
			
			HashMap<String, String> examErrors=new HashMap<>();
			Exam exam=new Exam();
			exam.setSubject(request.getParameter("subject"));
			exam.setDate(request.getParameter("date"));
			
			examErrors=validator.examValidate(exam);
			session.setAttribute("examErrors", examErrors);
			session.setAttribute("exam", exam);
			
			if(examErrors.isEmpty()) {
				dao.addExam(exam);
				response.sendRedirect("../exam/all");
			}else {
				response.sendRedirect("../createExam.jsp");
			}
			
			
			
		}
		Exam exam=(Exam)session.getAttribute("exam");
		
		if(info.endsWith("setSubject")) {
			
			
			
			String subject=request.getParameter("title");
			HashMap<String, String> errors=validator.subjectValidate(subject);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				dao.updateSubject(exam.getId(),subject);
				response.sendRedirect("../exam/show?id="+exam.getId());
			}else {
				session.setAttribute("examErrors", errors);
				response.sendRedirect("../exam/show?id="+exam.getId());
			}
			
		}
        if(info.endsWith("setDate")) {
			
			String date=request.getParameter("title");
			HashMap<String, String> errors=validator.dateValidate(date);
			if(errors==null) errors=new HashMap<>();
			if(errors.isEmpty()) {
				dao.updateDate(exam.getId(),date);
				response.sendRedirect("../exam/show?id="+exam.getId());
			}else {
				session.setAttribute("examErrors", errors);
				response.sendRedirect("../exam/show?id="+exam.getId());
			}
			
			
		}
        if(info.endsWith("delete")) {
        	
        	dao.removeExamById(exam.getId());
        	session.setAttribute("exam", null);
        	response.sendRedirect("../exam/all");
        	
        }
	}

}
