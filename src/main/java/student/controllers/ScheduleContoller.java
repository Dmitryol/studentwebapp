package student.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import student.bean.Event;
import student.dao.EventValidator;
import student.dao.Schedule;
import student.dao.ScheduleDatabaseImpl;



/**
 * Servlet implementation class SheduleContoller
 */
@WebServlet("/schedule/*")
public class ScheduleContoller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScheduleContoller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String info=request.getPathInfo();
		HttpSession session = request.getSession();
		Schedule data=new ScheduleDatabaseImpl();
		
		if(info.endsWith("show")) {
			
			List<Event> events=new ArrayList<>();
			events=data.getAllEvents();
			
			session.setAttribute("events", events);
			
			response.sendRedirect("../schedule.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String info=request.getPathInfo();
		HttpSession session=request.getSession();
		EventValidator validator=new EventValidator();
		Schedule data=new ScheduleDatabaseImpl();
		
		if(info.endsWith("addEvent")) {
			
			Event event=new Event(); 
			event.getStudent().setId(Integer.parseInt(request.getParameter("student")));
			event.getExam().setId(Integer.parseInt(request.getParameter("exam")));
			event.setStatus(request.getParameter("status"));
			event.setScore(Integer.parseInt(request.getParameter("score")));
			
			HashMap<String, String> errors=new HashMap<String, String>();
			errors=validator.validate(event);
			if(errors.isEmpty()) {
				data.addEvent(event);
			}else {
				if(errors.containsKey("scoreCannotExist"))  session.setAttribute("eventErrors", "current score cannot exist for this status");
				if(errors.containsKey("eventAlreadyExist"))  session.setAttribute("eventErrors", "this event already exist");
				if(errors.containsKey("score"))  session.setAttribute("eventErrors", "score is wrong" );
				if(errors.containsKey("status"))  session.setAttribute("eventErrors", "status is wrong" );
				if(errors.containsKey("exam"))  session.setAttribute("eventErrors", "this exam does not exist" );
				if(errors.containsKey("student"))  session.setAttribute("eventErrors", "this student does not exist" );
				
				
			}
			response.sendRedirect("../schedule/show");
			
		}
		if(info.endsWith("deleteEvent")) {
			
			int id=Integer.parseInt(request.getParameter("id"));
			data.deleteEventById(id);
			response.sendRedirect("../schedule/show");
		}
	}

}
