package student.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import student.bean.User;
import student.dao.UserDao;
import student.dao.UserDatabaseImpl;
import student.dao.UserValidator;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//private final String ADMIN="admin";
	//private final String PWD="qwest123";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UserValidator validator=new UserValidator();
		String login=request.getParameter("login");
		String password=request.getParameter("password");
		
		HashMap<String, String> errors=validator.isExist(login, password);
		
		if(errors.isEmpty()) {
			
			HttpSession session = request.getSession();
			UserDao data=new UserDatabaseImpl();
			User user=data.getUserByName(login);
			session.setAttribute("user_name", user.getLogin());
			session.setAttribute("user_status", user.getStatus() );
			//setting session to expiry in 30 mins
			session.setMaxInactiveInterval(30*60);
			response.sendRedirect("welcome.jsp");
		}else {
			RequestDispatcher rd=getServletContext().getRequestDispatcher("/login.html");
			PrintWriter out= response.getWriter();
			if(errors.containsKey("login")) out.println("<font color=red>Login is empty.</font>");
			if(errors.containsKey("Password")) out.println("<font color=red>Password is empty.</font>");
			if(errors.containsKey("logIn")) out.println("<font color=red>User name or password is wrong.</font>");
			rd.include(request, response);
		}
    }
		
}
