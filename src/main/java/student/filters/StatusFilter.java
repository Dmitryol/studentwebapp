package student.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class StatusFilter
 */
@WebFilter("/StatusFilter")
public class StatusFilter implements Filter {

    /**
     * Default constructor. 
     */
    public StatusFilter() {
        
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req=(HttpServletRequest)request;
		HttpServletResponse resp=(HttpServletResponse)response;
		
		String uri=req.getRequestURI();
		String info=req.getPathInfo();
		
		HttpSession session=req.getSession(false);
		
		if(session!=null) {
			if(!session.getAttribute("user_status").equals("admin")&&(uri.endsWith("user.jsp")
					||uri.endsWith("users.jsp")
					||uri.endsWith("createUser.jsp"))) {
				resp.sendRedirect("welcome.jsp");
			}
			if(!session.getAttribute("user_status").equals("admin")&&info!=null&&(uri.contains("user"))) {
				resp.sendRedirect("../welcome.jsp");
			}
			if(session.getAttribute("user_status").equals("student")&&info==null&&!(uri.endsWith("welcome.jsp")||uri.endsWith("schedule.jsp"))) {
				resp.sendRedirect("welcome.jsp");
			}
			if(session.getAttribute("user_status").equals("student")&&info!=null&&!uri.endsWith("schedule/show")) {
				resp.sendRedirect("../welcome.jsp");
			}
			
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
