package student.dao;

import java.util.HashMap;

import student.bean.Student;

public class StudentValidator {
	
	private ValidateProperties properties=new ValidateProperties();
	
	public StudentValidator() {
		
	}
	
	public HashMap<String, String> validateStudent(Student stud) {
		
		HashMap<String,String> errorMap=new HashMap<>();
		validateName(stud.getName(), errorMap);
		validateSurname(stud.getSurname(),errorMap);
		validateGroup(stud.getGroup(), errorMap);
		return errorMap;
		
	}
	public HashMap<String,String> validateName(String name){
		HashMap<String,String> errorMap=new HashMap<>();
		validateName(name, errorMap);	
		return errorMap;
		
	}
	public HashMap<String,String> validateSurname(String surname){
		HashMap<String,String> errorMap=new HashMap<>();
		validateSurname(surname,errorMap);
		return errorMap;
		
	}
	public HashMap<String,String> validateGroup(String group){
		HashMap<String,String> errorMap=new HashMap<>();
		validateGroup(group, errorMap);
		return errorMap;
		
	}


	private void validateName(String name, HashMap<String, String> errors) {
		
		if(name.length() > properties.getMaxStudentName()) errors.put("name", "name is long");
		if(name.length() < properties.getMinStudentName()) errors.put("name", "name is short");
		if(name==null || name.trim().isEmpty())errors.put("name", "name is empty");
		
	}
	private void validateSurname(String surname, HashMap<String,String> errors) {
	
		if(surname.length() > properties.getMaxStudentName()) errors.put("surname", "surname is long");
		if(surname.length() < properties.getMinStudentSurname()) errors.put("surname", "surname is short");
		if(surname==null || surname.trim().isEmpty())errors.put("surname", "surname is empty");
		
	}
	private void validateGroup(String group, HashMap<String, String> errors) {
		
		if(group.length() > properties.getMaxStudentName()) errors.put("group", "group is long");
		if(group.length() < properties.getMinStudentGroup()) errors.put("group", "group is short");
		if(group==null || group.trim().isEmpty())errors.put("group", "group is empty");
		
	}

}
