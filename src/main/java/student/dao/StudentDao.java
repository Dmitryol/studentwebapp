package student.dao;

import java.util.List;

import student.bean.Student;

public interface StudentDao {
	
	public void createStudent(Student student);
	public void removeById(int id);
	public Student getById(int id);
	public List<Student> getAll();
	public void updateName(int id, String arg);
	public void updateSurname(int id, String arg);
	public void updateGroup(int id, String arg);

}
