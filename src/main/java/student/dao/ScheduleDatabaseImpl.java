package student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import student.bean.Event;
import student.bean.Exam;
import student.bean.Student;

public class ScheduleDatabaseImpl implements Schedule {
	
	private ComboPooledDataSource pool;
	private PoolInit config=new PoolInit();
	
	public ScheduleDatabaseImpl() {
		
		pool=config.getPool();
	}

	@Override
	public void addEvent(Event event) {
		
		String sql="INSERT INTO schedule_exams (student_id, exam_id, status, score) values ("+
				event.getStudent().getId()+","+event.getExam().getId()+",'"+event.getStatus()+"',"+event.getScore()+");";
		Connection conn=null;
		Statement st=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			st.execute(sql);
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public Event getEvent(int id) {
		
		return null;
	}

	@Override
	public List<Event> getAllEvents() {
	    
		String sql="SELECT * FROM schedule_exams";
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		List<Event> events = new ArrayList<>();
		HashMap<Integer, Student> students=new HashMap<>();
		HashMap<Integer, Exam> exams=new HashMap<>();
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			students=getStudents(conn);
			exams=getExams(conn);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				Event event=new Event();
				event.setId(rs.getInt(1));
				event.setStudent(students.get(rs.getInt(2)));
				event.setExam(exams.get(rs.getInt(3)));
				event.setStatus(rs.getString(4));
				event.setScore(rs.getInt(5));
				events.add(event);
			}
			conn.commit();
			
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return events;
	}

	

	@Override
	public void updateStudentId(int id, int studentId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateExamId(int id, int examId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateStatus(int id, String status) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateScore(int id, int score) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteEventById(int id) {
		
		String sql="DELETE FROM schedule_exams WHERE id="+id+";";
		Connection con=null;
		Statement st=null;
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
	}
	private HashMap<Integer, Student> getStudents(Connection conn) {
		
		String sql="SELECT * FROM students;";
		Statement st=null;
		ResultSet rs=null;
		HashMap<Integer, Student> students=new HashMap<>();
		
		try {
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()){
				Student stud=new Student();
				stud.setId(rs.getInt(1));
				stud.setName(rs.getString(2));
				stud.setSurname(rs.getString(3));
				stud.setGroup(rs.getString(4));
				students.put(stud.getId(), stud);
			}
			
		} catch (SQLException e) {
			students.clear();
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return students;
	}
private HashMap<Integer, Exam> getExams(Connection conn) {
		
		String sql="SELECT * FROM exams";
		Statement st=null;
		ResultSet rs=null;
		HashMap<Integer, Exam> exams=new HashMap<>();
		
		try {
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()){
				Exam exam=new Exam();
				exam.setId(rs.getInt(1));
				exam.setSubject(rs.getString(2));
				exam.setDate(rs.getString(3));
				exams.put(exam.getId(), exam);
			}
			
		} catch (SQLException e) {
			exams.clear();
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return exams;
	}

}
