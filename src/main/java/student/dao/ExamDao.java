package student.dao;

import java.util.List;

import student.bean.Exam;

public interface ExamDao {
	
	public void addExam(Exam exam);
	public void removeExamById(int id);
	public Exam getExamById(int id);
	public List<Exam> getAllExam();
	public void updateSubject(int id, String subject);
	public void updateDate(int id, String date);

}
