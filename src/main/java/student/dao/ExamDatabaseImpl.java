package student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import student.bean.Exam;

public class ExamDatabaseImpl implements ExamDao {
	
	private ComboPooledDataSource pool;
	private PoolInit config=new PoolInit();
	
	public ExamDatabaseImpl() {
		pool=config.getPool();
	}

	@Override
	public void addExam(Exam exam) {
		
		String sql="INSERT INTO exams (subject, date) VALUES ('"+exam.getSubject()+"', '"+exam.getDate()+"');";
	    Connection con=null;
	    Statement st=null;
	
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
	}

	@Override
	public void removeExamById(int id) {
		
		String sql="DELETE FROM exams WHERE id="+id+";";
		Connection con=null;
		Statement st=null;
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Exam getExamById(int id) {
		
		String sql="SELECT * FROM exams WHERE id='"+id+"'";
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		Exam exam=new Exam();
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				exam.setId(rs.getInt(1));
				exam.setSubject(rs.getString(2));
				exam.setDate(rs.getString(3));
			
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return exam;
	}
	

	@Override
	public List<Exam> getAllExam() {
		
		String sql="SELECT * FROM exams";
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		List<Exam> exams=new ArrayList<>();
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				Exam exam=new Exam();
				exam.setId(rs.getInt(1));
				exam.setSubject(rs.getString(2));
				exam.setDate(rs.getString(3));
				exams.add(exam);
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return exams;
	}

	@Override
	public void updateSubject(int id, String subject) {
	    
		update(id, "subject", subject);
	}

	@Override
	public void updateDate(int id, String date) {
		
		update(id, "date", date); 
		
	}
	private void update(int id, String type, String value) {
		
		String sql="UPDATE exams SET "+type+" = '"+value+"' WHERE id = "+id;
		Connection con=null;
		Statement st=null;
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
