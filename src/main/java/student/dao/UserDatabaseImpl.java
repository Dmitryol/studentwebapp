package student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import student.bean.User;

public class UserDatabaseImpl implements UserDao {
	
	private ComboPooledDataSource pool;
	private PoolInit config=new PoolInit();
	
	public UserDatabaseImpl() {
		
		pool=config.getPool();
	}

	@Override
	public User getUserByName(String name) {
		
		String sql="SELECT * FROM users WHERE login='"+name+"'";
		User user=new User();
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				user.setLogin(rs.getString(1));
				user.setPassword(rs.getString(2));
				user.setStatus(rs.getString(3));
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
		return user;
	}

	@Override
	public List<User> getUsers() {
		
		String sql="SELECT * FROM users";
		List<User> users=new ArrayList<User>();
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				User user=new User();
				user.setLogin(rs.getString(1));
				user.setPassword(rs.getString(2));
				user.setStatus(rs.getString(3));
				users.add(user);
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
		return users;
	}

	@Override
	public void createUser(User user) {
		
		String sql="INSERT INTO users (login, password, status) VALUES ('"+user.getLogin()+"','"+user.getPassword()+"','"+user.getStatus()+"')";
		Connection conn=null;
		Statement st=null;
	
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			st.execute(sql);
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void deleteUserByName(String name) {
		
		String sql="DELETE FROM users WHERE login ='"+name+"'";
		Connection conn=null;
		Statement st=null;
	
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			st.execute(sql);
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void loginUpdate(String login, String new_login) {
		
		update("UPDATE users SET login='"+new_login+"' WHERE login='"+login+"'");
		
	}

	@Override
	public void passwordUpdate(String login, String new_password) {
		
		update("UPDATE users SET password='"+new_password+"' WHERE logig='"+login+"'");
		
	}
	@Override
	public void statusUpdate(String login, String status) {
	
		update("UPDATE users SET status='"+status+"' WHERE login='"+login+"'");
	}
	private void update(String sql) {
		
		Connection conn=null;
		Statement st=null;
	
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			st.execute(sql);
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
		
}
