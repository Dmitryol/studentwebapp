package student.dao;

import java.util.List;

import student.bean.User;

public interface UserDao {
	
	public User getUserByName(String name);
	public List<User> getUsers();
	public void createUser(User user);
	public void loginUpdate(String login, String new_login);
	public void passwordUpdate(String login, String password );
	public void statusUpdate(String login, String status);
	public void deleteUserByName(String name);

}
