package student.dao;

import java.util.HashMap;

import student.bean.Exam;

public class ExamValidator {
	
    ValidateProperties properties=new ValidateProperties(); 
	
	public ExamValidator() {
		
	}
	public HashMap<String, String> examValidate(Exam exam){
		
		HashMap<String, String>examErrors=new HashMap<>();
		subjectValidate(examErrors, exam.getSubject());
		dateValidate(examErrors, exam.getDate());
		return examErrors;
	}
	private void subjectValidate(HashMap<String, String> errors, String subject) {
		
		
		if(subject.length()<properties.getMinExamSubject()) errors.put("subject", "title is short");
		if(subject==null||subject.trim().isEmpty()) errors.put("subject", "tatle is empty");
		if(subject.length()>properties.getMaxExamSubject()) errors.put("subject", "title is long");
		
	}
	private void dateValidate(HashMap<String, String> errors, String date) {
		
		if(date==null||date.trim().isEmpty()) errors.put("date", "date is empty");
	}
	public HashMap<String, String> subjectValidate(String subject){
		
		HashMap<String, String>examErrors=new HashMap<>();
		subjectValidate(examErrors, subject);
		return examErrors;
	}
	public HashMap<String, String> dateValidate(String date){
		
		HashMap<String, String>examErrors=new HashMap<>();
		dateValidate(examErrors, date);
		return examErrors;
	}

}
