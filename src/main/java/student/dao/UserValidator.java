package student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import student.bean.User;

public class UserValidator {
	
	private ComboPooledDataSource pool;
	private PoolInit config=new PoolInit();
	private ValidateProperties properties=new ValidateProperties();
	
	public UserValidator() {
		
		pool=config.getPool();
	}
	
	public HashMap<String, String> userValidate(User user){
		
		HashMap<String, String> errors=new HashMap<String, String>();
		loginValidate(user.getLogin(), errors);
		passwordValidate(user.getPassword(),errors);
		statusValidate(user.getStatus(), errors);
		return errors;
	}
	public HashMap<String, String> loginValidate(String login){
		
		HashMap<String, String> errors=new HashMap<String, String>();
		loginValidate(login, errors);
		return errors;
		
	}
    public HashMap<String, String> passwordValidate(String password){
		
		HashMap<String, String> errors=new HashMap<String, String>();
		passwordValidate(password,errors);
		return errors;
		
	}
    public HashMap<String, String> statusValidate(String status){
    	
    	HashMap<String, String> errors=new HashMap<String, String>();
		statusValidate(status,errors);
		return errors;
    	
    }
	public HashMap<String, String> isExist(String login, String password){
		
		HashMap<String, String> errors=new HashMap<String, String>();
	    HashMap<String, String> loginErrors=new HashMap<String, String>();
		loginValidate(login, loginErrors);
		if(loginErrors.isEmpty()) {
			errors.put("logIn","login or password is wrong");
		}else {
			if(loginErrors.get("login").equals("login alredy exist")) isPassword(login, password, errors);
			if(loginErrors.get("login").equals("login is empty")) errors.put("login","login is empty");
		}
		return errors;
	}
	private void loginValidate(String login, HashMap<String, String> errors) {
		
		String sql="SELECT COUNT(login) FROM users WHERE login='"+login+"';";
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		if(login.length()<properties.getMinUserName()) errors.put("login", "login is short");
		if(login==null||login.trim().isEmpty()) errors.put("login","login is empty");
		if(login.length()>properties.getMaxUserName()) errors.put("login", "login is long");
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				int count=rs.getInt(1);
				if(count==1) errors.put("login","login alredy exist");
			}
			conn.commit();
			} catch (SQLException e) {
			    e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	private void passwordValidate(String pwd, HashMap<String, String> errors) {
		
		if(pwd.length()<properties.getMinUserPassword()) errors.put("pwd", "password is short");
		if(pwd==null||pwd.trim().isEmpty()) errors.put("pwd","password is empty");
		if(pwd.length()>properties.getMaxUserPassword()) errors.put("pwd", "password is long");
	
	}
	private void statusValidate(String status, HashMap<String, String> errors) {
		
		if(status.length()<properties.getMinUserStatus()) errors.put("status", "status is short");
		if(status==null||status.trim().isEmpty()) errors.put("status", "status is empty");
		if(status.length()>properties.getMaxUserStatus()) errors.put("status", "status is long");
		if(status.equals("admin")&& status.equals("examinator")&&status.equals("student")==false) errors.put("status", "status is wrong");
	}
	private void isPassword(String login, String pwd, HashMap<String, String> errors) {
		
		passwordValidate(pwd,errors);
		
		String sql="SELECT password FROM users WHERE login='"+login+"'";
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				if(rs.getString(1).equals(pwd)==false) errors.put("logIn", "login or password is wrong");
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
