package student.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ValidateProperties {
	
	private static final String PATH="student/dao/validate.properties";
	private static Properties config;
	
	public ValidateProperties() {
		
		getProperties();
		
	}
	private void getProperties() {
		ClassLoader loader =Thread.currentThread().getContextClassLoader();
		InputStream in =loader.getResourceAsStream(PATH);
		
		try {	
		    config = new Properties();
			config.load(in);
		
		}
		 catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public int getMinUserName() {
		
		return Integer.parseInt(config.getProperty("userNameMin"));
	}
    public int getMaxUserName() {
		
		return Integer.parseInt(config.getProperty("userNameMax"));
	}
    public int getMinUserPassword() {
		
		return Integer.parseInt(config.getProperty("userPasswordMin"));
	}
    public int getMaxUserPassword() {
		
		return Integer.parseInt(config.getProperty("userStatusMax"));
	}
    public int getMinUserStatus() {
		
		return Integer.parseInt(config.getProperty("userStatusMin"));
	}
    public int getMaxUserStatus() {
		
		return Integer.parseInt(config.getProperty("userPasswordMax"));
	}
    public int getMinStudentName() {
		
		return Integer.parseInt(config.getProperty("studentNameMin"));
	}
    public int getMaxStudentName() {
		
		return Integer.parseInt(config.getProperty("studentNameMax"));
	}
    public int getMinStudentSurname() {
		
		return Integer.parseInt(config.getProperty("studentSurnameMin"));
	}
    public int getMaxStudentSurname() {
		
		return Integer.parseInt(config.getProperty("studentSurnameMax"));
	}
    public int getMinStudentGroup() {
		
		return Integer.parseInt(config.getProperty("studentGroupMin"));
	}
    public int getMaxStudentGroup() {
		
		return Integer.parseInt(config.getProperty("studentGroupMax"));
	}
    public int getMinExamSubject() {
		
		return Integer.parseInt(config.getProperty("examSubjectMin"));
	}
    public int getMaxExamSubject() {
		
		return Integer.parseInt(config.getProperty("examSubjectMax"));
	}  
}
