package student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import student.bean.Student;

public class StudDatabaseImpl implements StudentDao {
	
	
	private ComboPooledDataSource pool;
	private PoolInit config=new PoolInit();


	public StudDatabaseImpl() {
		
		
		pool=config.getPool();
	}

	@Override
	public void createStudent(Student student) {

		String sql="INSERT INTO students (name, surname, group_number) VALUES ('"
		                                  +student.getName()+"', '"
				                          +student.getSurname()+"', '"
		                                  +student.getGroup()+"');";
	    Connection con=null;
	    Statement st=null;
	
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	@Override
	public void removeById(int id) {
		
		String sql="DELETE FROM students WHERE id="+id+";";
		Connection con=null;
		Statement st=null;
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public Student getById(int id) {

		String sql="SELECT * FROM students WHERE id='"+id+"'";
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		Student stud=new Student();
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				stud.setId(rs.getInt(1));
				stud.setName(rs.getString(2));
				stud.setSurname(rs.getString(3));
				stud.setGroup(rs.getString(4));
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("id: "+stud.getId());
		return stud;
	}

	@Override
	public List<Student> getAll() {
		
		String sql="SELECT * FROM students";
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		List<Student> students=new ArrayList<Student>();
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				Student stud=new Student();
				stud.setId(rs.getInt(1));
				stud.setName(rs.getString(2));
				stud.setSurname(rs.getString(3));
				stud.setGroup(rs.getString(4));
				students.add(stud);
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return students;
	}
	
    private void update(int id, String type, String value) {
		
		String sql="UPDATE students SET "+type+" = '"+value+"' WHERE id = "+id;
		Connection con=null;
		Statement st=null;
		
		try {
			con=pool.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(1);
			st=con.createStatement();
			st.execute(sql);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				st.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}


	@Override
	public void updateName(int id, String arg) {
		
		update(id, "name", arg);
		
	}

	@Override
	public void updateSurname(int id, String arg) {
		
		update(id, "surname", arg); 
		
	}

	@Override
	public void updateGroup(int id, String arg) {
		
		update(id, "group_number", arg);
		
	}



}
