package student.dao;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class PoolInit {
	
	private static final String PATH="student/dao/db.properties";
	private static Properties config;
	private static ComboPooledDataSource pool=new ComboPooledDataSource();
	
	public PoolInit() {
		getProperties();
		initPool(5,50,1);
	}
	
	public ComboPooledDataSource getPool() {
		return pool;
	}
	
	private void getProperties() {
		ClassLoader loader =Thread.currentThread().getContextClassLoader();
		InputStream in =loader.getResourceAsStream(PATH);
		
		try {	
		    config = new Properties();
			config.load(in);
		
		}
		 catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	private void initPool(int min, int max, int increment) {
		
	try {	
		pool.setDriverClass(config.getProperty("driver"));
		pool.setJdbcUrl(config.getProperty("url"));
		pool.setUser(config.getProperty("user"));
		pool.setPassword(config.getProperty("password"));
		pool.setMinPoolSize(min);
		pool.setAcquireIncrement(increment);
		pool.setMaxPoolSize(max);
	} catch (PropertyVetoException e) {
		e.printStackTrace();
	}
		
	}
	public static Properties getProp() {
		return config;
	}

}
