package student.dao;

import java.util.List;

import student.bean.Event;

public interface Schedule {
	
	public void addEvent(Event event);
	public Event getEvent(int id);
	public List<Event> getAllEvents();
	public void updateStudentId(int id, int studentId);
	public void updateExamId(int id,int examId);
	public void updateStatus(int id, String status);
	public void updateScore(int id, int score);
	public void deleteEventById(int id);

}
