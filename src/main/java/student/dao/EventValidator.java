package student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import student.bean.*;

public class EventValidator {
	
	private ComboPooledDataSource pool;
	private PoolInit config=new PoolInit();
	
	public EventValidator() {
		
		pool=config.getPool();
	}
	public HashMap<String, String> validate(Event event){
		
		HashMap<String, String> errors=new HashMap<String, String>();
		studentValidate(event.getStudent(), errors);
		examValidate(event.getExam(), errors);
		statusValidate(event.getStatus(),errors);
		scoreValidate(event.getScore(),errors);
		if(event.getScore()>0 && event.getStatus().equals("passed")==false) errors.put("scoreCannotExist", "current score cannot exist for this status");
		isEventExist(event.getStudent(), event.getExam(), errors);
		return errors;
		
	}
	private void studentValidate(Student stud, HashMap<String, String> errors) {
		
		String sql="SELECT COUNT(id) FROM students WHERE id='"+stud.getId()+"';";
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				int count=rs.getInt(1);
				if(count==0) errors.put("stud","this student does not exist");
			}
			conn.commit();
			} catch (SQLException e) {
			    e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	private void examValidate(Exam exam, HashMap<String, String> errors) {
		
		String sql="SELECT COUNT(id) FROM exams WHERE id='"+exam.getId()+"';";
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				int count=rs.getInt(1);
				if(count==0) errors.put("exam","this exam does not exist");
			}
			conn.commit();
			} catch (SQLException e) {
			    e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	private void statusValidate(String status, HashMap<String, String> errors) {
		
		if(status.equals("admitted")||
		   status.equals("not admited")||
		   status.equals("passed")||
		   status.equals("not passed")) {
			
		}else {errors.put("status", "status is wrong"); }
		
		
	}
	private void scoreValidate(int score, HashMap<String, String> errors) {
		
		if(score<0||score>5) errors.put("score", "score is wrong");
		
	}
	private void isEventExist(Student stud, Exam exam, HashMap<String, String> errors) {
		
		String sql="SELECT COUNT(id) FROM schedule_exams WHERE student_id='"+stud.getId()+"' AND exam_id='"+exam.getId()+"';";
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		
		try {
			conn=pool.getConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(1);
			st=conn.createStatement();
			rs=st.executeQuery(sql);
			while(rs.next()) {
				int count=rs.getInt(1);
				if(count==1) errors.put("eventAlreadyExist","event already exist");
			}
			conn.commit();
			} catch (SQLException e) {
			    e.printStackTrace();
		}finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
