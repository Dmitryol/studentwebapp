CREATE TABLE schedule_exams(
id INT PRIMARY KEY DEFAULT NEXTVAL('schedule_increment'),
student_id INT REFERENCES students(id),
exam_id INT REFERENCES exams(id),
status VARCHAR(20) NOT NULL,
score INT);