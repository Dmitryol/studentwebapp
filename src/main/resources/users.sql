CREATE TABLE users(
login VARCHAR(15) PRIMARY KEY,
password VARCHAR(16) NOT NULL,
status VARCHAR(10)NOT NULL);

INSERT INTO users VALUES 
('admin','qwest123','admin'),
('dmitry','1234','examinator'),
('stud','1111','student');