CREATE TABLE students(
id INT NOT NULL DEFAULT NEXTVAL('stud_increment') PRIMARY KEY,
name VARCHAR(15) NOT NULL,
surname VARCHAR(20) NOT NULL,
group_number VARCHAR(20) NOT NULL); 